package com.example.arch;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.example.arch.utilities.AppExecutors;

public class ApplicationLoader extends Application {

    public static volatile Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        if (applicationContext == null) {
            applicationContext = getApplicationContext();
        }
    }
}


