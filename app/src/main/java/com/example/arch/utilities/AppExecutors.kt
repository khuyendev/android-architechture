package com.example.arch.utilities

import android.os.Handler
import android.os.Looper


import java.util.concurrent.Executor
import java.util.concurrent.Executors

class AppExecutors private constructor() {

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }

    companion object {

        private var mDiskIO: Executor = Executors.newSingleThreadExecutor();

        private var mNetworkIO: Executor = Executors.newFixedThreadPool(3);

        private var mMainThread: Executor = MainThreadExecutor();

        fun diskIO(): Executor {
            return mDiskIO
        }

        fun networkIO(): Executor {
            return mNetworkIO
        }

        fun mainThread(): Executor {
            return mMainThread
        }

    }

}
