package com.example.arch;

import androidx.appcompat.app.AppCompatActivity;
import jp.wasabeef.glide.transformations.BlurTransformation;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

public class MainActivity extends AppCompatActivity {
    String base = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoKCgoKCgsMDAsPEA4QDxYUExMUFiIYGhgaGCIzICUgICUgMy03LCksNy1RQDg4QlFFeT0pPXnFlZXGPiI+7u/sBCgoKCgoKCwwMCw8QDhAPFhQTExQWIhgaGBoYIjMgJSAgJSAzLTcsKSw3LVFAODhAUV5PSk9ecWVlcY+Ij7u7+//CABEIAB8AKgMBIgACEQEDEQH/xAAaAAACAwEBAAAAAAAAAAAAAAAFBgECAwAE/9oACAEBAAAAAOvjXygG3M2hGIc0wYVfNRisZL6z/8QAFwEBAAMAAAAAAAAAAAAAAAAAAQACA//aAAgBAhAAAADO0B//xAAYAQACAwAAAAAAAAAAAAAAAAAABAECA//aAAgBAxAAAACdKMiv/8QAJhAAAQQCAQIGAwAAAAAAAAAAAQIDBBEAEgUTMQYhQVFhgRQWwf/aAAgBAQABPwDgORXx8zSUwHG3a6qCKI1TW2cz4tnfkmnHWY5J6aUEDD4m5ORLMppbqlNarQ2myKzlJYlOBTOwsk6k9io3j87kTEcjCStDam0o6TYGlX2wtqHkUov1sHHIPKzXVS5MRyM7sSSBt3+VZ+vuzFXupYu9bQTnE8VG4qO4XmWi4sHfZG1IHoSclPvSpTy1RG47ZXYSk0oAdr1yPE5V1Dboj2yujv61hhSbNxln6Oc6pxSWnm5CEqbsFt3zbWD7+x+cc5hyGrdMZTbldysLR9KHcZIn8rMWkPqccDgNJCv5kHg5bpQt13opCgdEgEn4OMjpoCLJAAGbZJaS+CCLx/www8a6q0IJvUHIfFsRQmgpZCaClmzQxJCazbLPtn//xAAZEQADAQEBAAAAAAAAAAAAAAABAhEAEhD/2gAIAQIBAT8A7FmaKLRqDmUHcKDfP//EAB0RAAICAwADAAAAAAAAAAAAAAECAAMEETEQEmH/2gAIAQMBAT8ACs3FJlVFtraVDGQqxU9Ex8qyjY6p6sfMvdSvtofPH//Z";
    String link = "https://rollaclub.nyc3.digitaloceanspaces.com/monthly_07_2011/post-13888-0-02770100-1310705367.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imageView = findViewById(R.id.imageView);
        RequestOptions requestOptions = new RequestOptions().transform(new BlurTransformation(60));
        Glide.with(getApplicationContext()).load(link).thumbnail(Glide.with(getApplicationContext())
                .load(base).apply(requestOptions)).apply(new RequestOptions().dontTransform().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true
        )).into(imageView);
    }
}
