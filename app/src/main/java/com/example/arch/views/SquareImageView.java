package com.example.arch.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

public class SquareImageView extends AppCompatImageView {

    private int dx = 1;
    private int dy = 1;

    public SquareImageView(Context context) {
        super(context);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

//    val ratio = width / height.toFloat()
//                if (width > height)
//            mBinding.imgPhoto.set(1.0f, 1 / ratio)
//            else
//            mBinding.imgPhoto.set(1.0f / 2, 1 / (2 * ratio))
    public void set(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = getMeasuredWidth();
        setMeasuredDimension(w, w * dx / dy);
    }
}
