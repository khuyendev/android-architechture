package com.example.arch.data.repository;

import com.example.arch.data.NetworkBoundResource;
import com.example.arch.data.Resource;
import com.example.arch.data.local.dao.MovieDao;
import com.example.arch.data.local.entity.MovieEntity;
import com.example.arch.data.remote.model.MoviesResponse;
import com.example.arch.data.remote.service.MovieDBService;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import retrofit2.Call;

/**
 * Created by KhuyenN on 19/05/2017.
 */

public class MovieRepository implements Repository<MovieEntity> {

    private final MovieDao movieDao;
    private final MovieDBService movieDBService;

    public MovieRepository(MovieDao movieDao, MovieDBService movieDBService) {
        this.movieDao = movieDao;
        this.movieDBService = movieDBService;
    }

    public LiveData<MovieEntity> getMovie(int id) {
        return movieDao.getMovie(id);
    }

    @Override
    public LiveData<Resource<List<MovieEntity>>> load(int page, int size) {
        return new NetworkBoundResource<List<MovieEntity>, MoviesResponse>() {
            @Override
            protected void saveCallResult(@NonNull MoviesResponse item) {
                movieDao.saveMovies(item.getResults());
            }

            @NonNull
            @Override
            protected LiveData<List<MovieEntity>> loadFromDb() {
                return movieDao.loadMovies();
            }

            @NonNull
            @Override
            protected Call<MoviesResponse> createCall() {
                return movieDBService.loadMovies();
            }
        }.asLiveData();
    }

}
