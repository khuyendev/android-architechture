package com.example.arch.data.remote;

public class ApiConstants {
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final int TIMEOUT_IN_SEC = 15;
}
