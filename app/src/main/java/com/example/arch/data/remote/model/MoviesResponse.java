package com.example.arch.data.remote.model;


import com.example.arch.data.local.entity.MovieEntity;

import java.util.List;


/**
 * Created by mertsimsek on 19/05/2017.
 */

public class MoviesResponse {
    private List<MovieEntity> results;

    public List<MovieEntity> getResults() {
        return results;
    }

    public void setResults(List<MovieEntity> results) {
        this.results = results;
    }
}
