package com.example.arch.data.repository;

import com.example.arch.data.Resource;

import java.util.List;

import androidx.lifecycle.LiveData;

public interface Repository<T> {
    LiveData<Resource<List<T>>>  load(int page, int size);
}
