package com.example.arch.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Photo implements Parcelable {
    private String originUrl;
    private String largeUrl;
    private String mediumUrl;
    private String smallUrl;
    private String thumbnailUrl;
    private String thumbnailBase64;
    private int width;
    private int height;

    //origin
    //largeUrl for      FHD         1920x1080
    //mediumUrl for     HD          1280x720
    //smallUrl for      qHD         960x540
    //thumbnailUrl for  thumbnail   150x150
    //thumbnailBase64 for  thumbnail   42x42 data base64

    public String getOriginUrl() {
        return originUrl;
    }

    public void setOriginUrl(String originUrl) {
        this.originUrl = originUrl;
    }

    public String getLargeUrl() {
        return largeUrl;
    }

    public void setLargeUrl(String largeUrl) {
        this.largeUrl = largeUrl;
    }

    public String getMediumUrl() {
        return mediumUrl;
    }

    public void setMediumUrl(String mediumUrl) {
        this.mediumUrl = mediumUrl;
    }

    public String getSmallUrl() {
        return smallUrl;
    }

    public void setSmallUrl(String smallUrl) {
        this.smallUrl = smallUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getThumbnailBase64() {
        return thumbnailBase64;
    }

    public void setThumbnailBase64(String thumbnailBase64) {
        this.thumbnailBase64 = thumbnailBase64;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    protected Photo(Parcel in) {
        originUrl = in.readString();
        largeUrl = in.readString();
        mediumUrl = in.readString();
        smallUrl = in.readString();
        thumbnailUrl = in.readString();
        thumbnailBase64 = in.readString();
        width = in.readInt();
        height = in.readInt();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(originUrl);
        dest.writeString(largeUrl);
        dest.writeString(mediumUrl);
        dest.writeString(smallUrl);
        dest.writeString(thumbnailUrl);
        dest.writeString(thumbnailBase64);
        dest.writeInt(width);
        dest.writeInt(height);
    }
}
