package com.example.arch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.arch.utilities.AppExecutors;

import androidx.annotation.Nullable;

public class Splash extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppExecutors.Companion.mainThread().execute(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash.this, MainActivity.class));
                finish();
            }
        });
    }
}
