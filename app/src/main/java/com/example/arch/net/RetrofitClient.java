package com.example.arch.net;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.arch.data.remote.ApiConstants.BASE_URL;

public class RetrofitClient {
    private static OkHttpClient.Builder httpClient = null;
    private static Retrofit.Builder builder;


    public static <T> T getService(Class<T> service) {
        if (builder == null) {
            builder = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
        }
        if (httpClient == null) {
            httpClient = new OkHttpClient.Builder();
            httpClient.authenticator(new TokenAuthenticator());
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = null;
                    requestBuilder = original.newBuilder()
                            .method(original.method(), original.body());
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(service);
    }
}
