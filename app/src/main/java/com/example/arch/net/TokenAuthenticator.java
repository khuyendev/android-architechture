package com.example.arch.net;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/**
 * Created by KhuyenN
 * This is example Token Authenticator
 */
public class TokenAuthenticator implements Authenticator {

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
//        String userRefreshToken = "your refresh token";
//        String cid = "your client id";
//        String csecret = "your client secret";
//        String baseUrl = "your base url";
//
//        boolean refreshResult = refreshToken(baseUrl, userRefreshToken, cid, csecret);
//        if (refreshResult) {
//            //token moi cua ban day
//            String accessToken = "your new access token";
//
//            // thuc hien request hien tai khi da lay duoc token moi
//            return response.request().newBuilder().header("Authorization", accessToken).build();
//        } else {
//            //Khi refresh token failed ban co the thuc hien action refresh lan tiep theo
//            return null;
//        }
        return null;
    }

//    public boolean refreshToken(String url, String refresh, String cid, String csecret)
//            throws IOException {
//        URL refreshUrl = new URL(url + "token");
//        HttpURLConnection urlConnection = (HttpURLConnection) refreshUrl.openConnection();
//        urlConnection.setDoInput(true);
//        urlConnection.setRequestMethod("POST");
//        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//        urlConnection.setUseCaches(false);
//        String urlParameters = "grant_type=refresh_token&client_id="
//                + cid
//                + "&client_secret="
//                + csecret
//                + "&refresh_token="
//                + refresh;
//
//        urlConnection.setDoOutput(true);
//        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
//        wr.writeBytes(urlParameters);
//        wr.flush();
//        wr.close();
//
//        int responseCode = urlConnection.getResponseCode();
//
//        if (responseCode == 200) {
//            BufferedReader in =
//                    new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//            String inputLine;
//            StringBuffer response = new StringBuffer();
//
//            while ((inputLine = in.readLine()) != null) {
//                response.append(inputLine);
//            }
//            in.close();
//
//            // this gson part is optional , you can read response directly from Json too
//            Gson gson = new Gson();
//            RefreshTokenResult refreshTokenResult =
//                    gson.fromJson(response.toString(), RefreshTokenResult.class);
//
//            // handle new token ...
//            // save it to the sharedpreferences, storage bla bla ...
//            return true;
//        } else {
//            //cannot refresh
//            return false;
//        }
//    }
}
